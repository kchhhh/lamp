(function () {
    'use strict';

    angular.module('helloCoreApi').controller('lampListsCtrl', [
        '$scope',
        '$http',
        '$q',
        'c8yBase',
        'c8yEvents',
        'c8yCepModule',
        '$routeParams',
        'c8yInventory',
        '$cacheFactory',
        'c8yDevices',
        'c8yMeasurements',
        '$rootScope',
        'c8yRealtime',
        'c8yDeviceControl',
        lampListsCtrl
    ]);
    function lampListsCtrl($scope, $http, $q, c8yBase, c8yEvents, c8yCepModule, $routeParams, c8yInventory, $cacheFactory, c8yDevices, c8yMeasurements, $rootScope, c8yRealtime, c8yDeviceControl) {


        c8yDevices.list().then(function (devices) {
            $scope.devices = [];
            angular.forEach(devices, function (device) {
                $scope.devices.push(device);
                // console.log($scope.devices);

            });

            // console.log($scope.devices);
        });
        c8yMeasurements.list(
            angular.extend(c8yBase.timeOrderFilter(), {
                // type: 'c8y_smartLEDMeasurement'
            })
        ).then(function (measurements) {
            $scope.measurements = measurements;
            // console.log($scope.measurements);
        });
        c8yMeasurements.list(
            angular.extend(c8yBase.timeOrderFilter(), {})
        ).then(function (measurements) {
            $scope.measurements = measurements;
            // console.log($scope.measurements);
            angular.forEach($scope.measurements, function (data) {
                // $scope.smart.push(data.c8y_smartLEDMeasurement);
                if (data.c8y_smartLEDMeasurement) {
                    $scope.smart = data.c8y_smartLEDMeasurement;
                    $scope.power = $scope.smart.Power.value;
                    // console.log($scope.power);
                    $scope.current = $scope.smart.Current.value;
                    // console.log($scope.current);
                    $scope.voltage = $scope.smart.Voltage.value;
                    // console.log($scope.voltage);
                }
            })

        });
            // c8yDevices.save({
            //     "id": 12235,
            //     "com_goldenpool_model_smartDevice": {
            //         "name": "sub",
            //         "parameters": {
            //             "STBD": 100,
            //             "PWM": 50
            //         }
            //     }
            // });
            // var operation = {
            //     "deviceId": "12235",
            //     "com_goldenpool_model_smartDevice": {
            //         "name": "smart device Control",
            //         "parameters": {
            //             "STBD": 100,
            //             "PWM": 50,
            //             "other": "tbd"
            //         }
            //     },
            //     "description": "smart device control"
            // };
            // c8yDeviceControl.create(operation);

        // c8yDeviceControl.list().then(function (operations) {
        //     $scope.operations = [];
        //     angular.forEach(operations, function (operation) {
        //         $scope.operations.push(operation);
        //         console.log($scope.operations);
        //     });
        // });
        var deviceId = 12235;
        c8yDevices.detail(deviceId).then(function (res) {
            $scope.device = res.data;
            console.log($scope.device);
        });


        $scope.isShow = true;
        $scope.getDevicesId = function (id) {
            $scope.deviceId = id;
            c8yDevices.detail($scope.deviceId).then(function (res) {
                $scope.device = res.data;
                // console.log(res.data);
                // $scope.stbd = $scope.device.com_goldenpool_model_smartDevice.parameters.STBD;
                // $scope.pwm = $scope.device.com_goldenpool_model_smartDevice.parameters.PWM;
                // console.log($scope.stbd);
                $scope.setSave = function () {
                    // c8yDevices.save({
                    //     "id": $scope.deviceId,
                    //     "com_goldenpool_model_smartDevice": {
                    //         "name": "sub",
                    //         "parameters": {
                    //             "STBD": parseInt($scope.stbd),
                    //             "PWM": parseInt($scope.pwm)
                    //         }
                    //     }
                    // });
                    // var operation = {
                    //     "deviceId": $scope.deviceId,
                    //     "com_goldenpool_model_smartDevice": {
                    //         "name": "smart device Control",
                    //         "parameters": {
                    //             "STBD": $scope.stbd,
                    //             "PWM": $scope.pwm,
                    //             "other": "tbd"
                    //         }
                    //     },
                    //     "description": "smart device control"
                    // };
                    var operation = {
                        "deviceId": "12111",
                        "com_goldenpool_model_smartDevice": {
                            "name": "smart device Control",
                            "parameters": {
                                "STBD": 'cc',
                                "PWM": 'cc',
                                "other": "tbd"
                            }
                        },
                        "description": "smart device control"
                    };
                    c8yDeviceControl.create(operation);

                    c8yDeviceControl.list().then(function (operations) {
                        $scope.operations = [];
                        angular.forEach(operations, function (operation) {
                            $scope.operations.push(operation);
                            console.log($scope.operations);
                        });
                    });
                }
            });
            $scope.isShow = false;
        };
        $scope.returnLists = function () {
            $scope.isShow = true;
        };

    }

})();

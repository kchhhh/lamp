(function () {
    'use strict';

    angular.module('helloCoreApi').controller('powerCtrl', [
        '$scope',
        'c8yBase',
        'c8yRealtime',
        '$http',
        '$rootScope',
        '$q',
        'c8yDeviceControl',
        'c8yInventory',
        '$cacheFactory',
        'c8yDevices',
        'c8yCepModuleExamples',
        '$timeout',
        'c8yCepModule',
        powerCtrl
    ]);

    function powerCtrl($scope, c8yBase, c8yRealtime, $http, $rootScope, $q, c8yDeviceControl, c8yInventory, $cacheFactory, c8yDevices, c8yCepModuleExamples, $timeout, c8yCepModule) {
        c8yDevices.list().then(function (devices) {
            $scope.devices = [];
            angular.forEach(devices, function (device) {
                $scope.devices.push(device);
                console.log($scope.devices);
            });
        });

        // var moId = 10317;
        // c8yInventory.detail(moId).then(function (res) {
        //     $scope.demo = res.data;
        // $scope.Rated = res.data.c8y_PowerAccumulation.Rated;
        // $scope.Actual = res.data.c8y_PowerAccumulation.Actual;
        // $scope.total = Math.round(($scope.Rated - $scope.Actual)/$scope.Rated*100);
        $scope.Rated = 55260;
        $scope.Actual = 25187;
        $scope.total = Math.round(($scope.Rated - $scope.Actual) / $scope.Rated * 100);
        // console.log($scope.total);
        // console.log($scope.Rated);
        // console.log($scope.Actual);
        $(function () {
            $('#container').highcharts({
                chart: {
                    backgroundColor: '#eeeeee',
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: "节约功耗:" + $scope.total + '%',
                    style: {
                        color: '#000000',
                        fontWeight: 'bolder'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: false,
                        depth: 35
                    }
                },
                tooltip: {
                    animation: false,
                    pointFormat: ""
                },
                series: [{
                    type: 'pie',
                    data: [
                        ['额定功耗', $scope.Rated],
                        ['实际功耗', $scope.Actual]
                    ]
                }],
                colors: ['#FF272A', '#2EFF27']
            });
        });
        // });

        $(function () {
            $(document).ready(function () {
                Highcharts.setOptions({
                    global: {
                        useUTC: false
                    }
                });
                $('#container2').highcharts({
                    chart: {
                        type: 'spline',
                        animation: Highcharts.svg, // don't animate in old IE
                        marginRight: 10,
                        backgroundColor:'#eeeeee',
                        events: {
                            load: function () {
                                // set up the updating of the chart each second
                                var series = this.series[0];
                                setInterval(function () {
                                    var x = (new Date()).getTime(), // current time
                                        y = 50;
                                    series.addPoint([x, y], true, true);
                                }, 1000);
                            }
                        }
                    },
                    title: {
                        text: 'Measurements'
                    },
                    xAxis: {
                        type: 'datetime',
                        tickPixelInterval: 50
                    },
                    yAxis: { //设置Y轴
                        title: '',
                        max: 300, //Y轴最大值
                        min: 0  //Y轴最小值
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.series.name + '</b><br/>' +
                                Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                                Highcharts.numberFormat(this.y, 2);
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    series: [{
                        name: 'Measurement',
                        data: (function () {
                            // generate an array of random data
                            var data = [],
                                time = (new Date()).getTime(),
                                i;
                            for (i = -19; i <= 0; i += 1) {
                                data.push({
                                    x: time + i * 1000,
                                    y: 100
                                });
                            }
                            return data;
                        }())
                    }]
                });
            });
        });


    }


})();
